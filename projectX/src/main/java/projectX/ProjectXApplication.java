package projectX;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectXApplication {

	public static DeveloperPanelConnector con;
	
	public static void main(String[] args) throws IOException {
		con = new DeveloperPanelConnector(9999);
//		new MessageHandler(9500);
		SpringApplication.run(ProjectXApplication.class, args);		
	}

}
