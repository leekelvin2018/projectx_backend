package projectX.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import projectX.handlers.AccessDeniedHandlerImp;
import projectX.handlers.AuthenticationEntryPointImp;
import projectX.handlers.AuthenticationFailureHandlerImp;
import projectX.handlers.AuthenticationSuccessHandlerImp;
import projectX.handlers.LogoutSuccessHandlerImp;
import projectX.services.UserDetailSecuritySerivce;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled =true)
public class SecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private UserDetailSecuritySerivce userDetailService;
	
	@Autowired
	private AuthenticationEntryPointImp authenticationEntryPointImpl;
	
	@Autowired
	private AccessDeniedHandlerImp accessDeniedHandlerImpl;
	
	@Autowired
	private AuthenticationSuccessHandlerImp authenticationSuccessHandlerImpl;
	
	@Autowired
	private AuthenticationFailureHandlerImp authenticationFailureHandlerImpl;
	
	@Autowired
	private LogoutSuccessHandlerImp logoutSuccessHandlerImpl;
	
	public SecurityConfig() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception {

		http
		.csrf().disable()
		.cors().and()
			.authorizeRequests().antMatchers("/login").permitAll()
			.and().authorizeRequests().antMatchers("/register",
					"/register/*"
					,"/register/*/*/*/*/*").permitAll()			
				.and().authorizeRequests().antMatchers("/message", "/message/*").permitAll()
				.and().authorizeRequests().antMatchers("/check").permitAll().filterSecurityInterceptorOncePerRequest(false)
				.anyRequest().authenticated().and()
				.exceptionHandling().accessDeniedHandler(accessDeniedHandlerImpl).and()
				.exceptionHandling().authenticationEntryPoint(authenticationEntryPointImpl).and()
			.formLogin()
			.usernameParameter("username")
			.passwordParameter("password")
			.failureHandler(authenticationFailureHandlerImpl)
			.successHandler(authenticationSuccessHandlerImpl)
			.and()
			.logout()
            .permitAll()
            .logoutUrl("/logout")
            .logoutSuccessHandler(logoutSuccessHandlerImpl)
            .and()
			.rememberMe();//.httpBasic();
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new TempEncoder();
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailService).passwordEncoder(this.passwordEncoder());
	}
	
	@Bean
    public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedOrigin("*"); // You should only set trusted site here. e.g. http://localhost:4200 means only this site can access.
        configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE","HEAD","OPTIONS"));
        configuration.addAllowedHeader("*");
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}

class TempEncoder implements PasswordEncoder{

	@Override
	public String encode(CharSequence rawPassword) {
		// TODO Auto-generated method stub
		return rawPassword.toString();
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		// TODO Auto-generated method stub
		return rawPassword.toString().equals(encodedPassword);
	}
	
}
