package projectX.services;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import projectX.Utility;
import projectX.beans.Absence;
import projectX.beans.User;
import projectX.beans.UserOffense;
import projectX.beans.UserReportStatus;
import projectX.daos.AbsenceDao;
import projectX.daos.UserDao;
import projectX.daos.UserOffenseDao;
import projectX.daos.UserReportStatusDao;

@Service
public class ManagementService {

	@Autowired
	private AbsenceDao ad;
	
	@Autowired
	private UserDao ud;
	
	@Autowired
	private UserReportStatusDao ursd;
	
	@Autowired
	private UserOffenseDao uod;
	
	public ManagementService() {
		// TODO Auto-generated constructor stub
	}

	//on loading
	public List<CustomizedUserInformations> getAllExceptAdmin(){
		try {
			List<User> usersExceptAdmin = ud.findAll();
			int length = usersExceptAdmin.size();
			int index = 0;
			//remove admin from the list
			while(index < length) {
				if(usersExceptAdmin.get(index).getProfiles().get(0).getType().equals("ROLE_ADMIN")) {
					usersExceptAdmin.remove(index);
					index--;
					length--;
				}
				index++;
			}
			List<CustomizedUserInformations> users = new LinkedList<CustomizedUserInformations>();
			index = 0;
			//move the items from User list to CUstomizedUserInformation list
			while(index < length) {
				users.add(new CustomizedUserInformations());
				users.get(index).setId(usersExceptAdmin.get(index).getId());
				users.get(index).setFirstName(usersExceptAdmin.get(index).getFirst_name());
				users.get(index).setLastName(usersExceptAdmin.get(index).getLast_name());
				index++;
			}
			length = usersExceptAdmin.size();
			index = 0;
			UserReportStatus report = null;
			Calendar time = Calendar.getInstance();
			int day = time.get(Calendar.DAY_OF_MONTH);
			int month = time.get(Calendar.MONTH);
			String reportTime =month +"-"+day;
			//set user information based on need
			while(index< length) {
				users.get(index).setHasPendingAbsRequest(this.hasPendingRequest(usersExceptAdmin.get(index).getUsername()));
				report = ursd.findByDateAndId(reportTime, usersExceptAdmin.get(index).getId());
				if(report != null) {
					users.get(index).setMorningIn(report.getFirstin() == 1);
					users.get(index).setMorningOut(report.getFirstout() == 1);
					users.get(index).setAfternoonIn(report.getSecondin() == 1);
					users.get(index).setAfternoonOut(report.getSecondout() == 1);
				}
				index++;				
			}
			return users;
		}
		catch(Exception e) {
			System.out.println("ManagementService.getAllExceptAdmin(void)");
			e.printStackTrace();
			return null;
		}
	}
	
	//not on loading
	public List<Absence> getAllAbsRecordByEmployeeId(int id) {
		try {
			return ad.findAllRequestByEmployeeId(id);
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("ManagementService.getAllAbsRecordByEmployeeId(int)=> ERORR");
			return null;
		}
	}
	
	//on loading
	public boolean hasPendingRequest(String un) {
		try {
			User u = ud.findUserByUsername(un);
			
			
			List<Absence> records = ad.findAllRequestByEmployeeId(u.getId());
			int size = records.size();
			int index = 0;
			while(index< size) {
				if(records.get(index).getStatus() == 0) {
					return true;
				}
				index++;
			}
			return false;
		}
		catch(Exception e) {
			return false;
		}
	}
	
	//not on loading
	public String updateRequest(int id, short decision) {
		try {
			Absence requestRecord = ad.findById(id).get();
			requestRecord.setStatus(decision);
			ad.save(requestRecord);
			return "{\"report\":\"update successful\"}";
		}
		catch(Exception e) {
			System.out.println("ManagementService.updateRequest");
			return "{\"error\":\"absence status update failure\"}";
		}
	}
	
	//not on loading
	public List<UserOffense> getReport(int employeeId) {
		try {
			return uod.findAllOffensesByEmployeeId(employeeId);
		}
		catch(Exception e) {
			return null;
		}
	}
	
	//not on loading
	public String updateOffenseNote(int id,int penalty, String note) {
		try {
			UserOffense uf = uod.findById(id).get();
			uf.setBehaviorsummary(note);
			uf.setPenalty(penalty);
			uod.save(uf);
			return "{\"report\":\"offense update done\"}";
		}
		catch(Exception e) {
			System.out.println("ManagementService.updateOffenseNote");
			return "{\"error\":\"offense note and penalty update failure\"}";
		}
	}

	
	public boolean helpClockIn(int eid, int when, int option) {
		try {
			UserReportStatus report = ursd.findByDateAndId(Utility.parseTodayDate(),eid);
			if( report == null) {
				report = new UserReportStatus(Utility.parseTodayDate(),eid,0,0,0,0);
			}
			if(when == 1) {
				report.setFirstin(option);
			}
			else if(when == 2) {
				report.setFirstout(option);
			}
			else if(when == 3) {
				report.setSecondin(option);
			}
			else {
				report.setSecondout(option);
			}
			return ursd.save(report) != null;
		}
		catch( Exception e) {
			return false;
		}
	}
	
/*detailed design:
 * 0, need to see all operators on loading (id, username, first name, last name, clock in/out state) 
 * 1, need to let admin pull all absence request of an operator by last name. 
 * 1.1, need to let admin know if a person has pending request on loading
 * 2, need to allow admin to approve/reject pending request. 
 * 3, need to allow admin to delete rejected request? (this may not make sense)
 * 4, need to allow admin to see everyone's report status on loading
 * 5, need to allow admin to clock a person out
 * 6, need to allow admin to see a person's offense record, and make notes if necessary
 * front end: a calendar to display sick leaves?
 * */

	public class CustomizedUserInformations{
		protected int id;
		protected String firstName;
		protected String lastName;
		protected boolean hasPendingAbsRequest = false;
		protected boolean morningIn = false;
		protected boolean morningOut = false;
		protected boolean afternoonIn = false;
		protected boolean afternoonOut= false;
		public CustomizedUserInformations() {}
		public CustomizedUserInformations(int id, String firstName, String lastName, boolean moringIn, boolean morningOut,
				boolean afternoonIn, boolean afternoonOut) {
			super();
			this.id = id;
			this.firstName = firstName;
			this.lastName = lastName;
			this.morningIn = moringIn;
			this.morningOut = morningOut;
			this.afternoonIn = afternoonIn;
			this.afternoonOut = afternoonOut;
		}
		public CustomizedUserInformations(int id, String firstName, String lastName, boolean hasPendingAbsRequest,
				boolean moringIn, boolean morningOut, boolean afternoonIn, boolean afternoonOut) {
			super();
			this.id = id;
			this.firstName = firstName;
			this.lastName = lastName;
			this.hasPendingAbsRequest = hasPendingAbsRequest;
			this.morningIn = moringIn;
			this.morningOut = morningOut;
			this.afternoonIn = afternoonIn;
			this.afternoonOut = afternoonOut;
		}
		public boolean isHasPendingAbsRequest() {
			return hasPendingAbsRequest;
		}
		public void setHasPendingAbsRequest(boolean hasPendingAbsRequest) {
			this.hasPendingAbsRequest = hasPendingAbsRequest;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public boolean isMorningIn() {
			return morningIn;
		}
		public void setMorningIn(boolean moringIn) {
			this.morningIn = moringIn;
		}
		public boolean isMorningOut() {
			return morningOut;
		}
		public void setMorningOut(boolean morningOut) {
			this.morningOut = morningOut;
		}
		public boolean isAfternoonIn() {
			return afternoonIn;
		}
		public void setAfternoonIn(boolean afternoonIn) {
			this.afternoonIn = afternoonIn;
		}
		public boolean isAfternoonOut() {
			return afternoonOut;
		}
		public void setAfternoonOut(boolean afternoonOut) {
			this.afternoonOut = afternoonOut;
		}
		@Override
		public String toString() {
			return "CustomizedUserInformations [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName
					+ ", moringIn=" + morningIn + ", morningOut=" + morningOut + ", afternoonIn=" + afternoonIn
					+ ", afternoonOut=" + afternoonOut + "]";
		}
	
	}
}