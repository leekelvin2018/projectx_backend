package projectX.services;

import java.time.LocalTime;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import projectX.beans.UserOffense;
import projectX.daos.UserOffenseDao;

@Service
public class UserOffenseService {

	@Autowired
	private UserOffenseDao uod;
	public UserOffenseService() {
		// TODO Auto-generated constructor stub
	}

	public boolean logOffense(int id) {
		
		return null != uod.save(
				new UserOffense(0,id,0,Calendar.getInstance().toInstant().toString())
				);
		
	}
	
	public int getOffenseCount(int id) {
		return (int) uod.countById(id);
	}
}
