package projectX.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import projectX.beans.User;
import projectX.daos.UserDao;

@Service
public class UserService {

	@Autowired
	private UserDao ud;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}
	
	public Optional<User> findById(int id) {
		return this.ud.findById(id);
	}
	
	public User findByUsername(String un) {
		return this.ud.findUserByUsername(un);
	}
	
	public void save(User u) {
		System.out.println(ud.save(u));	
	}
	
	public List<User> getAll() {
		return this.ud.findAll();
	}

}
