package projectX.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import projectX.beans.User;
import projectX.daos.UserDao;

@Service
public class UserDetailSecuritySerivce implements UserDetailsService{
	@Autowired
	private UserDao ud;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		
		User u = ud.findUserByUsername(username);
		if(u == null) {
			throw new UsernameNotFoundException("User not found");
		}
		return u;
	}

}
