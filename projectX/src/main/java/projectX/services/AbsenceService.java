package projectX.services;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import projectX.beans.Absence;
import projectX.beans.User;
import projectX.daos.AbsenceDao;
import projectX.daos.UserDao;

@Service
public class AbsenceService {

	@Autowired
	private AbsenceDao ad;
	
	@Autowired
	private UserDao ud;
	public AbsenceService() {
		// TODO Auto-generated constructor stub
	}

	public List<Absence> getByLastname(String ln) {
		User u = ud.findByLastname(ln);
		List<Absence> requests = ad.findAllRequestByEmployeeId(u.getId());
		return requests;
	}
	
	public boolean requestAbsence(String un,String reason, String start, String end) {
		try {
			System.out.println("AbsenceService=> username  received:" + un);
			User u= ud.findUserByUsername(un);
			Object o = ad.save(new Absence(-1,u.getId(),start,end, reason,(short) 0));
			return o != null;
		}
		catch(NullPointerException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public List<AbsenceInfoForCalendar> getAllApproved(){
		try {
			List<AbsenceInfoForCalendar> allApproved = new LinkedList<AbsenceInfoForCalendar>();
			List<User> all = ud.findAll();
			//check everyone's leave request
			for(int i = 0; i < all.size(); i++) {
				List<Absence> approved = ad.findAllApprovedRequestByEmployeeId(all.get(i).getId());
				if(approved.size() != 0) {//if a person has approved leave request, put it on this year's calendar
					for(int j = 0; j < approved.size(); j++) {
						allApproved.add(new AbsenceInfoForCalendar(
								all.get(i).getFirst_name(),
								all.get(i).getLast_name(),
								approved.get(j).getStart_date(),
								approved.get(j).getEnd_date()
								));
					}
				}
			}
			return allApproved.size() != 0 ? allApproved : null;
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static class AbsenceInfoForCalendar{
		String firstName;
		String lastName;
		String start;
		String end;
		public AbsenceInfoForCalendar() {}
		public AbsenceInfoForCalendar(String firstName, String lastName, String start, String end) {
			super();
			this.firstName = firstName;
			this.lastName = lastName;
			this.start = start;
			this.end = end;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getStart() {
			return start;
		}
		public void setStart(String start) {
			this.start = start;
		}
		public String getEnd() {
			return end;
		}
		public void setEnd(String end) {
			this.end = end;
		}
		
	}
	
}
