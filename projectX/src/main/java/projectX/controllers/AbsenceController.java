package projectX.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import projectX.beans.Absence;
import projectX.services.AbsenceService;
import projectX.services.AbsenceService.AbsenceInfoForCalendar;

@RestController
@RequestMapping("absence")
public class AbsenceController {

	@Autowired
	private AbsenceService as;
	
	public AbsenceController() {
		// TODO Auto-generated constructor stub
	}

	@PostMapping
	@PreAuthorize("hasAnyRole('ROLE_OPERATOR')")
	public boolean requestAbsence(@RequestBody AbsenceContent request) {
		System.out.println(request);
		return as.requestAbsence(request.username, request.reason, request.start, request.end);
	}
	
	@GetMapping("/{lastname}")
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
	public List<Absence> getAbsence(@PathVariable("lastname")String ln){
		return as.getByLastname(ln);
	}
	
	@GetMapping("/all_approved")
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
	public List<AbsenceInfoForCalendar> getAllApprovedRequest() {
		return as.getAllApproved();
	}
	
}



class AbsenceContent{
	String username;
	String reason;
	String start;
	String end;
	public AbsenceContent() {
		super();
	}

	public AbsenceContent(String username, String reason, String start, String end) {
		super();
		this.username = username;
		this.reason = reason;
		this.start = start;
		this.end = end;
	}
	@Override
	public String toString() {
		return "AbsenceContent [username=" + username + ", reason=" + reason + ", start=" + start + ", end=" + end
				+ "]";
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	
	
}