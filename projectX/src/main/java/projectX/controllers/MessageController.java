package projectX.controllers;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import projectX.beans.User;
import projectX.services.EmailService;
import projectX.services.UserService;

@RestController
@RequestMapping("/message")
public class MessageController {

	@Autowired
	private EmailService es;
	
	@Autowired
	private UserService us;
	 
	public MessageController() {
		// TODO Auto-generated constructor stub
	}

	@PostMapping("/{who}")
	public boolean send(@RequestBody EmailContent ec, @PathVariable("who")String un) {
		System.out.println("MessageController=>"+ec);
		try {
			User u = us.findByUsername(un);
			es.sendSimpleMessage(ec.getEmail(), ec.getSubject(), ec.getContent()+"\r\nSent By:"+u.getFirst_name()+" "+u.getLast_name());
		}
		catch(Throwable e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@PostMapping
	public boolean receiveContactRequest(@RequestBody EmailContent ec) {
		try {
			es.sendSimpleMessage("guge1013635621@gmail.com","Client Contact", ec.getContent());
		}
		catch(Exception e) {
			System.out.println(e);
			return false;
		}
		return true;
	}
	
	@GetMapping
	public Object getAllContactInfo() {
		try {
			List<User> contacts = us.getAll();
			List<Contact> conInfo = new LinkedList<Contact>();
			for(int i = 0; i < contacts.size(); i++) {
				User u = contacts.get(i);
				Contact contact = new Contact(u.getFirst_name(),u.getLast_name(), u.getEmail() );
				conInfo.add(contact);
			}
			return conInfo;
		}
		catch(Exception e) {
			return "{\"error\": \"failed to get Contact\"}";
		}
	}
	
}

class EmailContent{
	private String content;
	private String email;
	private String subject;
	public EmailContent() {
		super();
		// TODO Auto-generated constructor stub
	}
	public EmailContent(String content, String email, String subject) {
		super();
		this.content = content;
		this.email = email;
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	@Override
	public String toString() {
		return "EmailContent [content=" + content + ", email=" + email + ", subject=" + subject + "]";
	}
	
	
	
}

class Contact{
	public String firstname;
	public String lastname;
	public String email;
	public Contact(String firstname, String lastname, String email) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
	}
	public Contact() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Contact [firstname=" + firstname + ", lastname=" + lastname + ", email=" + email + "]";
	}
	
	
}