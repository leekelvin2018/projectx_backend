package projectX.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import projectX.beans.User;
import projectX.security.Response;

@RestController
@RequestMapping("/check")
public class SessionController {

	
	
	public SessionController() {
		// TODO Auto-generated constructor stub
		
	}

	@GetMapping("/admin")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public Response isAdmin() {
		Authentication information = SecurityContextHolder.getContext().getAuthentication();
		Object un = information.getPrincipal();
        un = un.equals("anonymousUser") ? "anonymousUser" : ((User) un).getUsername();
		return new Response(true, 200, (String) un);
	}
	
	@GetMapping("/operator")
	@PreAuthorize("hasAnyRole('ROLE_OPERATOR')")
	public Response isOperator() {
		Authentication information = SecurityContextHolder.getContext().getAuthentication();
		Object un = information.getPrincipal();
        un = un.equals("anonymousUser") ? "anonymousUser" : ((User) un).getUsername();
        return new Response(true, 200, (String) un);
	}
	
}
