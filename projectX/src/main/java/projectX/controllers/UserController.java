package projectX.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import projectX.beans.User;
import projectX.services.UserService;

@RestController
@RequestMapping("/register")

public class UserController {
	
	@Autowired
	private UserService us;
	
	public UserController() {
		// TODO Auto-generated constructor stub
	}

	//this is a demonstration of how to receive it in parameter.
	@PostMapping("/{username}/{password}/{firstname}/{lastname}/{email}")
	public boolean save(@PathVariable("username") String un,
			@PathVariable("password")String pw,
			@PathVariable("firstname")String fn,
			@PathVariable("lastname")String ln,
			@PathVariable("email")String email) {
		System.out.println("at UserController:save=> " + un+ " "+ pw + " " + fn + " " + ln+" "+email);
		return true;
		//us.save(u);
	}
	
	
	
	

	@GetMapping("/{test}")
	public String save2(@PathVariable("test")String test) {
		return "test message";
	}
	
	@PostMapping
	@PreAuthorize("permitAll()")
	public boolean save(@RequestBody CustomObject u) {
		try {
			User checkUsername = us.findByUsername(u.username);
			if(checkUsername != null) {
				System.out.println("UserController=> registration rejected");
				return false;
			}
			User user = new User(u.getFirstname(),u.getLastname(),u.getUsername(),u.getPassword(),u.getEmail() );
			us.save(user);
			System.out.println("UserController=> registration accepted");
			return true;
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
}

class CustomObject{
	String username;
	String password;
	String firstname;
	String lastname;
	String email;
	
	public CustomObject() {}

	public CustomObject(String username, String password, String firstname, String lastname, String email) {
		super();
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "CustomObject [username=" + username + ", password=" + password + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", email=" + email + "]";
	}
	
	
	
	
}