package projectX.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import projectX.Utility;
import projectX.beans.Absence;
import projectX.beans.User;
import projectX.beans.UserOffense;
import projectX.beans.UserReportStatus;
import projectX.daos.UserDao;
import projectX.daos.UserOffenseDao;
import projectX.daos.UserReportStatusDao;
import projectX.services.ManagementService;
import projectX.services.ManagementService.CustomizedUserInformations;

@RestController
@RequestMapping("/ad_test")

public class AdminController {

	@Autowired
	private UserDao ud;
	@Autowired
	private UserOffenseDao uod;
	@Autowired
	private ManagementService ms;
	@Autowired
	private UserReportStatusDao ursd;
	public AdminController() {
		// TODO Auto-generated constructor stub
	}
	
	@GetMapping
	@PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
	public String displayAdminPage() {
		System.out.println("AdminController=> tried to display adminpage");
		return "{\"a\": \"Administrator: login successful\"}";
	}

	@PostMapping("/profile/{username}")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public User getAdmin(@PathVariable("username") String un) {
		System.out.println("AdminController=> check username: " + un);
		return ud.findUserByUsername(un);
	}
	
	@GetMapping("/profile/offense/{username}")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public int offenseCount(@PathVariable("username")String un) {
		User u = ud.findUserByUsername(un);
		return (int) uod.countById(u.getId());
	}
	
	@GetMapping("/manage")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public List<CustomizedUserInformations> getAllOperator(){
		return ms.getAllExceptAdmin();
	}
	
	@GetMapping("/manage/abs_record/{eid}")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public List<Absence> getAllAbsRecordByOperatorId(@PathVariable("eid")int id){
		List<Absence> check = ms.getAllAbsRecordByEmployeeId(id);
		System.out.println("AdminController=> check abs list: "+check);
		return check;
	}
	
	@GetMapping("/manage/abs_record/resolve/{eid}/{decision}")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public String resolveAbsRequest(@PathVariable("eid")int eid,@PathVariable("decision") int decision) {
		return ms.updateRequest(eid, (short) decision);
	}
	
	@GetMapping("/manage/offense_record/{eid}")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public List<UserOffense> getAllOffenseRecordByOperatorId(@PathVariable("eid")int eid){
		return ms.getReport(eid);
	}
	
	@GetMapping("/manage/clock_in_out/{eid}/{when}/{option}")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public boolean clockInOutOperator(@PathVariable("eid")int eid,@PathVariable("when") int when,@PathVariable("option") int option) {
		System.out.println("AdminController=> help clock in/out: " + eid+ " "+ when + " " + option);
		return ms.helpClockIn(eid, when, option);
	}
	
	@PostMapping("/manage/offense_record")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public String updateOffenseRecord(@RequestBody OffenseUpdate info) {
//		System.out.println("AdminController=> trying to daupte offense: "+info.toString());
		return ms.updateOffenseNote(info.id, info.penalty, info.note);
	}
	public static class OffenseUpdate{
		int id;
		int penalty;
		String note;
		public OffenseUpdate() {}
		public OffenseUpdate(int id, int penalty, String note) {
			super();
			this.id = id;
			this.penalty = penalty;
			this.note = note;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getPenalty() {
			return penalty;
		}
		public void setPenalty(int penalty) {
			this.penalty = penalty;
		}
		public String getNote() {
			return note;
		}
		public void setNote(String note) {
			this.note = note;
		}
		@Override
		public String toString() {
			return "OffenseUpdate [id=" + id + ", penalty=" + penalty + ", note=" + note + "]";
		};
		
	}
}
