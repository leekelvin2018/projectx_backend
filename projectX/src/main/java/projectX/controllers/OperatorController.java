package projectX.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import projectX.beans.Absence;
import projectX.beans.User;
import projectX.beans.UserOffense;
import projectX.daos.UserDao;
import projectX.daos.UserOffenseDao;
import projectX.services.AbsenceService;

@RestController
@RequestMapping("/op_test")
public class OperatorController {
	@Autowired
	private UserDao ud;
	
	@Autowired
	private AbsenceService as;
	
	@Autowired
	private UserOffenseDao uod;
	
	public OperatorController() {
		// TODO Auto-generated constructor stub
	}

	@GetMapping
	@PreAuthorize("hasAnyRole('ROLE_OPERATOR')")
	public String displayOperatorPage() {
		return "{\"a\": \"Operator: login successful\"}";
	}
	
	@PostMapping("/profile/{username}")
	@PreAuthorize("hasAnyRole('ROLE_OPERATOR')")
	public User getOperator(@PathVariable("username") String un) {
		System.out.println("OperatorController: check username: " + un);
		return ud.findUserByUsername(un);
	}
	
	@GetMapping("/profile/{username}")
	@PreAuthorize("hasAnyRole('ROLE_OPERATOR')")
	public List<Absence> getAbsRequests(@PathVariable("username") String un){
		User u = ud.findUserByUsername(un);
		return as.getByLastname(u.getLast_name());		
	}
	
	@GetMapping("/profile/offense/{username}")
	@PreAuthorize("hasAnyRole('ROLE_OPERATOR')")
	public int offenseCount(@PathVariable("username")String un) {
		User u = ud.findUserByUsername(un);
		return (int) uod.countById(u.getId());
	}
	
	@GetMapping("/profile/penalties/{username}")
	@PreAuthorize("hasAnyRole('ROLE_OPERATOR')")
	public int totalPenalty(@PathVariable("username")String un) {
		try {
			User u = ud.findUserByUsername(un);
			List<UserOffense> penaltyCheck = uod.findAllOffensesByEmployeeId(u.getId());
			int penalties = 0;
			if(penaltyCheck == null || penaltyCheck.size() == 0)
				return penalties;
			for(int i = 0; i < penaltyCheck.size(); i++) {
				penalties += penaltyCheck.get(i).getPenalty();
			}
			return penalties;
		}
		catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
}
