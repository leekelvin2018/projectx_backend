package projectX.controllers;

import java.sql.Time;
import java.time.LocalTime;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import projectX.DeveloperPanelConnector;
import projectX.ProjectXApplication;
import projectX.ProjectXDeveloperPanel;
import projectX.Utility;
import projectX.beans.User;
import projectX.beans.UserReportStatus;
import projectX.daos.UserDao;
import projectX.daos.UserReportStatusDao;
import projectX.services.UserOffenseService;

@RestController
@RequestMapping("/report")
public class UserReportStatusController {

	@Autowired
	private UserReportStatusDao report;
	
	@Autowired
	private UserDao ud;
	
	@Autowired
	private UserOffenseService uos;
	
	private LocalTime reportTime;
	
	private LocalTime morningReportTime = Time.valueOf("08:55:00").toLocalTime();
	private LocalTime morningReportTimeAdmin = Time.valueOf("08:00:00").toLocalTime();
	private LocalTime morningReportTimeDeadLine = Time.valueOf("09:00:00").toLocalTime();
	
	
	private LocalTime noonReportTime = Time.valueOf("11:45:00").toLocalTime();
	private LocalTime noonReportTimeAdmin = Time.valueOf("11:30:00").toLocalTime();
	private LocalTime noonReportTimeDeadLine = Time.valueOf("12:00:00").toLocalTime();
	
	private LocalTime noonReportOutTime = Time.valueOf("11:00:00").toLocalTime();
	
	private LocalTime afternoonReportOutTime = Time.valueOf("18:00:00").toLocalTime();
	

	
	public UserReportStatusController() {
		// TODO Auto-generated constructor stub
		System.out.println("UserReportStatusController.constructor=> devMod" + DeveloperPanelConnector.isDevMod()+ " devTime: " + DeveloperPanelConnector.getDevTime());
	}

	@PostMapping("/{username}")
	@PreAuthorize("hasAnyRole('ROLE_OPERATOR')")
	public String clockIn(@PathVariable("username") String un) {
		System.out.println("UserReportStatusController.clockIn=> developer mode: "  + DeveloperPanelConnector.isDevMod()+ " devTime: " + DeveloperPanelConnector.getDevTime());
		if(!DeveloperPanelConnector.isDevMod())// debug, and test purpose
			this.reportTime = LocalTime.now();
		else {
			this.reportTime = DeveloperPanelConnector.getDevTime();
		}
		User employee = ud.findUserByUsername(un);
		int id = employee.getId();
		String reportTime = Utility.parseTodayDate();
		
		if(this.reportTime.compareTo(morningReportTime) == -1) {
			return "{\"report\":\"Not Time To Clock In Yet!\"}";
		}//display this message if it failed
		
		if(!hasReportedIn(id,reportTime) || (DeveloperPanelConnector.isDevMod() && this.reportTime.compareTo(noonReportOutTime) == -1) ) {
			report.save(new UserReportStatus(
					reportTime,
					id,
					1,0,0,0 //means the clock in/out have not been performed
					));
			if(this.reportTime.compareTo(this.morningReportTimeDeadLine) == 1) {
				uos.logOffense(id);
				return "{\"report\":\"Untimely 9AM Clock In\"}";
			}
			return "{\"report\":\"9AM Clock In Received\"}";
		}
		else {
			if(this.reportTime.compareTo(noonReportTime) == -1) {
				return "{\"report\":\"Not Time To Clock In For Afternoon Yet!\"}";
			}
			UserReportStatus userReport = report.findByDateAndId(reportTime, id);
			userReport.setSecondin(1);
			report.save(userReport);
			if(this.reportTime.compareTo(noonReportTimeDeadLine) == 1) {
				uos.logOffense(id);
				return "{\"report\":\"Untimely 12AM Clock in\"}";
			}
			return "{\"report\":\"12AM Clock In Received\"}";
		}

	}
	
	@PostMapping("/admin/{username}")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public String adminClockIn(@PathVariable("username") String un) {
		System.out.println("UserReportStatusController.clockIn Admin=> developer mode: "  + DeveloperPanelConnector.isDevMod()+ " devTime: " + DeveloperPanelConnector.getDevTime());
		if(!DeveloperPanelConnector.isDevMod())// debug, and test purpose
			this.reportTime = LocalTime.now();
		else {
			this.reportTime = DeveloperPanelConnector.getDevTime();
		}
		User employee = ud.findUserByUsername(un);
		int id = employee.getId();

		String reportTime = Utility.parseTodayDate();
		
		if(this.reportTime.compareTo(morningReportTimeAdmin) == -1) {
			return "{\"report\":\"Not Time To Clock In Yet!\"}";
		}//display this message if it failed
		
		if(!hasReportedIn(id,reportTime) || (DeveloperPanelConnector.isDevMod() && this.reportTime.compareTo(noonReportOutTime) == -1) ) {
			report.save(new UserReportStatus(
					reportTime,
					id,
					1,0,0,0 //means the clock in/out have not been performed
					));
			if(this.reportTime.compareTo(this.morningReportTimeDeadLine) == 1) {
				uos.logOffense(id);
				return "{\"report\":\"Untimely 9AM Clock In\"}";
			}
			return "{\"report\":\"9AM Clock In Received\"}";
		}
		else {
			if(this.reportTime.compareTo(noonReportTimeAdmin) == -1) {
				return "{\"report\":\"Not Time To Clock In For Afternoon Yet!\"}";
			}
			UserReportStatus userReport = report.findByDateAndId(reportTime, id);
			userReport.setSecondin(1);
			report.save(userReport);
			if(this.reportTime.compareTo(noonReportTimeDeadLine) == 1) {
				uos.logOffense(id);
				return "{\"report\":\"Untimely 12AM Clock in\"}";
			}
			return "{\"report\":\"12AM Clock In Received\"}";
		}

	}
	
	@PostMapping("/out/{username}")
	@PreAuthorize("hasAnyRole('ROLE_OPERATOR','ROLE_ADMIN')")
	public String clockOut(@PathVariable("username")String un) {
		System.out.println("UserReportStatusController.clockOut=> developer mode: "  + DeveloperPanelConnector.isDevMod()+ " devTime: " + DeveloperPanelConnector.getDevTime());
		if(!DeveloperPanelConnector.isDevMod())// debug, and test purpose
			this.reportTime = LocalTime.now();
		else {
			this.reportTime = DeveloperPanelConnector.getDevTime();
		}
		User employee = ud.findUserByUsername(un);
		int id = employee.getId();
		Calendar time = Calendar.getInstance();
		int day = time.get(Calendar.DAY_OF_MONTH);
		int month = time.get(Calendar.MONTH);
		String reportTime =month +"-"+day;
		UserReportStatus userReport = report.findByDateAndId(reportTime, id);
		if(userReport == null || ( DeveloperPanelConnector.isDevMod() && this.reportTime.compareTo(morningReportTime) == -1)) {
			return "{\"report\":\"Not Clocked In Yet\"}";
		}
		if(userReport.getFirstout() == 0 ||( DeveloperPanelConnector.isDevMod() && this.reportTime.compareTo(noonReportTime) == -1) ) {
			try {
				userReport.setFirstout(1);
				report.save(userReport);
			}
			catch( NullPointerException e) {
				System.out.println("UserReportStatusController.clockOut=> Error handled deve mode. Report not initialized");
			}
			
			if(this.reportTime.compareTo(noonReportOutTime) == -1) {
				uos.logOffense(id);
				return "{\"report\":\"Untimely Morning Clockout\"}";
			}
			return "{\"report\":\"Morning Clockout Received\"}";
		}
		else if(userReport.getSecondout() == 0 ||  DeveloperPanelConnector.isDevMod()) {
			try {
				userReport.setSecondout(1);
				report.save(userReport);
			}
			catch( NullPointerException e) {
				System.out.println("UserReportStatusController.clockOut=> Error handled deve mode. Report not initialized");
			}
			if(this.reportTime.compareTo(afternoonReportOutTime) == -1) {
				uos.logOffense(id);
				return "{\"report\":\"Untimely Afternoon Clockou\"}";
			}
			return "{\"report\":\"Afternoon Clockout Received\"}";
		}
		return "{\"report\":\"Unknown Clockout Error\"}";
	}
	
	
	private boolean hasReportedIn(int id, String date) {
		try {
			UserReportStatus userReport = report.findByDateAndId(date, id);
			return userReport != null;
			}
		catch(Exception e) {
			System.out.println("error in status controller");
			return false;
		}
		
	}
	
	@GetMapping("/status/{username}")
	@PreAuthorize("hasAnyRole('ROLE_OPERATOR','ROLE_ADMIN')")
	public UserReportStatus getClockInStatus(@PathVariable("username")String username) {
		try {
			User u = ud.findUserByUsername(username);
			return report.findByDateAndId(Utility.parseTodayDate(), u.getId());
		}
		catch(Exception e) {
			return null;
		}
		
	}
	
}
