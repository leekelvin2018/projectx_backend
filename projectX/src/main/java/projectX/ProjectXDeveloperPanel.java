package projectX;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.Time;
import java.time.LocalTime;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class ProjectXDeveloperPanel extends JFrame {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7292015026623343399L;

	public static volatile LocalTime time;
	public static volatile boolean devMod = false;
	
	public ProjectXDeveloperPanel() throws HeadlessException {
		// TODO Auto-generated constructor stub
		setTitle("Dev Tool=>20:00:00");		
		setSize(400,400);
		JTextField time = new JTextField();
		getContentPane().add(time, "North");
		JButton devMod = new JButton("Dev Mode:"+ProjectXDeveloperPanel.devMod);
		devMod.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				ProjectXDeveloperPanel.devMod = !ProjectXDeveloperPanel.devMod;
				devMod.setText("Dev Mode:"+ProjectXDeveloperPanel.devMod);
				System.out.println("dev mod toggled:"+ProjectXDeveloperPanel.devMod);
			}
		});
		getContentPane().add(devMod, "Center");
		JButton setTime = new JButton("Set Time");
		setTime.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					ProjectXDeveloperPanel.time = Time.valueOf(time.getText()).toLocalTime();
					System.out.println("time set:" + ProjectXDeveloperPanel.time.toString());
					send();
				}
				catch(IllegalArgumentException e1) {
					JFrame error = new JFrame("Wrong Time input Format, Correct Format=> xx:xx:xx");
					error.setSize(1000, 10);
					error.setVisible(true);
				}
			}
			
		});
		getContentPane().add(setTime, "South");
		setVisible(true);
	}

	public ProjectXDeveloperPanel(GraphicsConfiguration gc) {
		super(gc);
		// TODO Auto-generated constructor stub
	}

	public ProjectXDeveloperPanel(String title) throws HeadlessException {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public ProjectXDeveloperPanel(String title, GraphicsConfiguration gc) {
		super(title, gc);
		// TODO Auto-generated constructor stub
	}

	public void send() {
		System.out.println("ProjectXDeveloperPanel.send()=> send called ");
		new Thread() {
			@Override
			public void run() {
				try(Socket sendDeveloperData = new Socket(InetAddress.getByName("localhost"),9999);
						DataOutputStream toSpringBoot = new DataOutputStream(sendDeveloperData.getOutputStream())) {	
					if(devMod) {
						toSpringBoot.writeBytes("1\n");
					}
					else {
						toSpringBoot.writeBytes("0\n");
					}
					toSpringBoot.flush();
					toSpringBoot.writeBytes(time.toString()+"\n");
					toSpringBoot.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}.start();
	}
	
	
	public static void main(String[] abs) {
		ProjectXDeveloperPanel devsTool = new ProjectXDeveloperPanel();
		devsTool.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	}
	
	
}
