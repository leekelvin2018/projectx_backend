package projectX.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import projectX.security.SecurityUtils;

@Component
public class AuthenticationEntryPointImp implements AuthenticationEntryPoint{

	@Override
	public void commence(HttpServletRequest req,
			HttpServletResponse res,
			AuthenticationException exception)
					throws IOException, ServletException{
		SecurityUtils.sendResponse(res, HttpServletResponse.SC_OK, "Authentication failed", exception);
	}

}
