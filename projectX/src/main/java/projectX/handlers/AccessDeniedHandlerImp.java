package projectX.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import projectX.security.SecurityUtils;

@Component
public class AccessDeniedHandlerImp implements AccessDeniedHandler{



	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		SecurityUtils.sendResponse(response, HttpServletResponse.SC_OK, "Not authorized resources", accessDeniedException);
	}
}