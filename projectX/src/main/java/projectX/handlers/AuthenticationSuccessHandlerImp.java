package projectX.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import projectX.security.SecurityUtils;
import projectX.services.UserService;

@Component
public class AuthenticationSuccessHandlerImp extends SimpleUrlAuthenticationSuccessHandler{
	@Autowired
	private UserService us;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest req,
			HttpServletResponse res,
			Authentication authentication)
				throws IOException, ServletException{
		System.out.println("Authen success: "+us.findByUsername(authentication.getName()).getProfiles());
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String role = us.findByUsername(authentication.getName()).getProfiles().get(0).getType();
		SecurityUtils.sendResponse(res, HttpServletResponse.SC_OK, us.findByUsername(authentication.getName()).getProfiles().toString(), null);
	}

}
