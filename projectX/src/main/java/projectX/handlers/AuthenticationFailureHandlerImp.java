package projectX.handlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import projectX.security.SecurityUtils;
@Component
public class AuthenticationFailureHandlerImp extends SimpleUrlAuthenticationFailureHandler{

	@Override
	public void onAuthenticationFailure(HttpServletRequest req,
			HttpServletResponse res,
			AuthenticationException e)
				throws IOException, ServletException{
		SecurityUtils.sendResponse(res, HttpServletResponse.SC_OK, "Login failed", e);
	}

}
