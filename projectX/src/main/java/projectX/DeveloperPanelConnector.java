package projectX;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.BindException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Time;
import java.time.LocalTime;

public class DeveloperPanelConnector extends ServerSocket {

	public static volatile boolean devMod;
	public static volatile LocalTime time;
	
	public DeveloperPanelConnector() throws IOException {
		// TODO Auto-generated constructor stub
	}

	public DeveloperPanelConnector(int port) throws IOException {
		this.createServerSocket(port);
		try(BufferedWriter bw = new BufferedWriter(new FileWriter("./src/main/java/resources/UserReportStatusResources.txt",false))) {
			bw.write("");
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

	public DeveloperPanelConnector(int port, int backlog) throws IOException {
		super(port, backlog);
		// TODO Auto-generated constructor stub
	}

	public DeveloperPanelConnector(int port, int backlog, InetAddress bindAddr) throws IOException {
		super(port, backlog, bindAddr);
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("resource")
	public boolean createServerSocket(int port) {
		System.out.println("trying to create server socket for developer panel");
		try{
			ServerSocket x = new ServerSocket(port);
		new Thread() {
			@Override
			public void run() {
				while(true) {
					Socket data;
					try {
						data = x.accept();
						setDeveloperOption(data);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			}
		}.start();
		}
		catch(BindException e) {
			System.out.println("DeveloperPanelConnector=> bind to port called again");
			return false;
		}
		catch(IOException e) {
			System.out.println("DeveloperPanelConnector=> IOException");
			return false;
		}
		
		return true;
	}
	
	
	public void setDeveloperOption(Socket data) {
		new Thread() {
			@Override
			public void run() {
				System.out.println("DeveloperPanelConnector=> trying to read data");
				try(BufferedReader devInput = new BufferedReader(new InputStreamReader(data.getInputStream()));
						BufferedWriter bw = new BufferedWriter(new FileWriter("./src/main/java/resources/UserReportStatusResources.txt",false))) {					
					String devMod = devInput.readLine();
					String localTimeInput = devInput.readLine();
					if(devMod.equals("1")) {
						DeveloperPanelConnector.devMod = true;
					}
					else {
						DeveloperPanelConnector.devMod = false;
					}
					DeveloperPanelConnector.time = Time.valueOf(localTimeInput).toLocalTime();
					bw.write(devMod+"\r\n"+ Time.valueOf(localTimeInput).toLocalTime().toString()+"\r\n");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("DeveloperPanelConnector.setDeveloperOption=> Error reading from Dev panel");
					devMod = false;
				}
				catch(Exception e) {
					e.printStackTrace();
					devMod = false;
				}
			}
		}.start();

	}
	
	public static boolean isDevMod() {
		try(BufferedReader br = new BufferedReader (new FileReader("./src/main/java/resources/UserReportStatusResources.txt"));) {			
			String devMod = br.readLine();
			return devMod.equals("1");
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		catch(IOException e) {
			e.printStackTrace();
			return false;
		}
		catch(NullPointerException e) {
			return false;
		}
	}
	
	public static LocalTime getDevTime() {
		try(BufferedReader br = new BufferedReader (new FileReader("./src/main/java/resources/UserReportStatusResources.txt"));) {			
			String devMod = br.readLine();
			String time = br.readLine();
			System.out.println(time);
			return Time.valueOf(time).toLocalTime();
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		catch(IOException e) {
			e.printStackTrace();
			return null;
		}
		catch(IllegalArgumentException e) {
			return null;
		}
	}
		
	

}
