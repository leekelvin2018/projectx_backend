package projectX;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.BindException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class MessageHandler extends ServerSocket {

	public MessageHandler() throws IOException {
		// TODO Auto-generated constructor stub
	}

	public MessageHandler(int port) throws IOException {
			this.openPort(port);			
	}

	public MessageHandler(int port, int backlog) throws IOException {
		super(port, backlog);
		// TODO Auto-generated constructor stub
	}

	public MessageHandler(int port, int backlog, InetAddress bindAddr) throws IOException {
		super(port, backlog, bindAddr);
		// TODO Auto-generated constructor stub
	}

	public boolean openPort(int port) {
		new Thread() {
			@SuppressWarnings("resource")
			@Override public void run() {
					try {
						while(true) {
							ServerSocket x = new ServerSocket(port);
							handleClient(x.accept());
						}
					}
					catch(BindException e) {
						System.out.println("MessageHandler=> bind to port called again");
						
					}
					catch(IOException e) {
						System.out.println("MessageHandler=> IOException");
						
					}
				}
			}.start();
		return true;
	}
	
	public void handleClient(Socket client) {
		new Thread() {
			@Override
			public void run() {
				try(BufferedReader fromWebPage = new BufferedReader(new InputStreamReader(client.getInputStream()));) {
					while(fromWebPage.ready()) {
						System.out.println("CONTENT FROM WEBSOCKET"+fromWebPage.readLine());
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}.start();
	}
	
}
