package projectX.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import projectX.beans.Absence;

public interface AbsenceDao extends JpaRepository<Absence, Integer> {

	@Query("select abs from Absence abs where abs.employee_id = ?1")
	public List<Absence> findAllRequestByEmployeeId(int id);
	
	@Query("select abs from Absence abs where abs.employee_id= ?1 and abs.status = 1")
	public List<Absence> findAllApprovedRequestByEmployeeId(int id);
}
