package projectX.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import projectX.beans.User;

public interface UserDao extends JpaRepository<User, Integer>{

	public User findUserByUsername(String username);

	@Query("from User where last_name=?1")
	public User findByLastname(String last_Name);
	
	
}
