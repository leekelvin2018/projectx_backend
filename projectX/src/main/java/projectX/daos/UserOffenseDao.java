package projectX.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import projectX.beans.UserOffense;

public interface UserOffenseDao extends JpaRepository<UserOffense, Integer> {

	@Query("select count(o) from UserOffense o where o.employee_id = ?1")
	public long countById(int id);
	
	@Query("select u from UserOffense u where u.employee_id = ?1")
	public List<UserOffense> findAllOffensesByEmployeeId(int id);
	
}
