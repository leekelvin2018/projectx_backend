package projectX.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import projectX.beans.UserReportStatus;

public interface UserReportStatusDao extends JpaRepository<UserReportStatus, String> {

	@Query("select r from UserReportStatus r where r.today=?1 and r.employee_id=?2")
	public UserReportStatus findByDateAndId(String date, int id);
	
	
//	@Query("update UserReportStatus r set r.firstout = 1 where r.employee_id = ?2 and r.today= ?1")
//	public boolean updateFirstClockOutByIdAndDate(String date,int id);
//	
//	@Query("update UserReportStatus r set r.secondout = 1 where r.employee_id = ?2 and r.today= ?1")
//	public boolean updateSecondClockOutByIdAndDate(String date,int id);
}
