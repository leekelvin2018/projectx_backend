package projectX.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="WOOSTER_BRUSH_OFFENSE")
public class UserOffense {
	@Id
	@SequenceGenerator(name = "wooster_brush_offense_sequence", sequenceName = "WOOSTER_BRUSH_OFFENSE_SEQ", allocationSize = 1)
	@GeneratedValue(generator = "wooster_brush_offense_sequence", strategy = GenerationType.AUTO)
	private int id;
	
	@Column
	private int employee_id;
	
	@Column
	private int penalty;
	
	@Column
	private String behaviorsummary;
	
	public UserOffense() {
		// TODO Auto-generated constructor stub
	}

	public UserOffense(int id, int employee_id, int penalty, String behaviorsummary) {
		super();
		this.id = id;
		this.employee_id = employee_id;
		this.penalty = penalty;
		this.behaviorsummary = behaviorsummary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}

	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(int penalty) {
		this.penalty = penalty;
	}

	public String getBehaviorsummary() {
		return behaviorsummary;
	}

	public void setBehaviorsummary(String behaviorsummary) {
		this.behaviorsummary = behaviorsummary;
	}

}
