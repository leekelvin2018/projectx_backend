package projectX.beans;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.*;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "WOOSTER_BRUSH_EMPLOYEE")
public class User implements UserDetails{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1013635621;

	@Id
	@SequenceGenerator(name = "wooster_brush_emp_sequence", sequenceName = "WOOSTER_BRUSH_EMP_SEQ", allocationSize = 1)
	@GeneratedValue(generator = "wooster_brush_emp_sequence", strategy = GenerationType.AUTO)
	private int id;

	@Column
	private String first_name;

	@Column
	private String last_name;

	@Column
	private String username;

	@Column
	private String password;
	
	@Column
	private String email;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "WOOSTER_BRUSH_EMPLOYEE_PROFILE",
			joinColumns = {
					@JoinColumn(name = "EMPLOYEE_ID", referencedColumnName = "ID")
			},
			inverseJoinColumns = {
					@JoinColumn(name = "PROFILE_ID", referencedColumnName = "ID")
			}
		)
	private List<Profile> profiles = new LinkedList<Profile>();

	public User() {
		super();
	}

	public User(String first_name, String last_name, String username, String password, String email) {
		this.first_name = first_name;
		this.last_name = last_name;
		this.username = username;
		this.password = password;
		this.email = email;
		profiles.add(new Profile(2,"ROLE_OPERATOR"));
	}
	
	public User(int id, String first_name, String last_name, String username, String password, String email, List<Profile> profiles) {
		this.id = id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.username = username;
		this.password = password;
		this.profiles = profiles;
		this.email = email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public List<Profile> getProfiles() {
		return profiles;
	}
	
	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	@Override
	public String toString() {
		return "User [id=" + id + ", first_name=" + first_name + ", last_name=" + last_name + ", username=" + username
				+ ", password=" + password +']';
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return this.profiles;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	
}
