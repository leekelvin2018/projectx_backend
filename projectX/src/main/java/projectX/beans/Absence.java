package projectX.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ABSENCEREQUEST")
public class Absence {

	@Id
	@SequenceGenerator(name = "abs_seq", sequenceName = "ABS_SEQ", allocationSize = 1)
	@GeneratedValue(generator = "abs_seq", strategy = GenerationType.AUTO)
	private int id;
	
	@Column
	private int employee_id;
	
	@Column
	private String start_date;
	
	@Column
	private String end_date;
	
	@Column
	private String reason;
	
	@Column
	private short status;
	
	public Absence() {
		// TODO Auto-generated constructor stub
	}

	public Absence(int id, int employee_id, String start_date, String end_date, String reason, short status) {
		super();
		this.id = id;
		this.employee_id = employee_id;
		this.start_date = start_date;
		this.end_date = end_date;
		this.reason = reason;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	
	
}
