package projectX.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;


@Entity
@Table(name="WOOSTER_BRUSH_PROFILE")
public class Profile implements GrantedAuthority{

	/**
	 * 
	 */
	private static final long serialVersionUID = 594129491;

	@Id
	private int id;
	
	@Column
	private String type;
	
	public Profile() {
		// TODO Auto-generated constructor stub
	}

	public Profile(int id, String type) {
		super();
		this.id = id;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Profile [id=" + id + ", type=" + type + "]";
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return type;
	}

	
}
