package projectX.beans;


import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="WOOSTER_BRUSH_REPORT_STATUS")
public class UserReportStatus {
	
	@Id
	private String today;
	
	@Column
	private int employee_id;
	
	@Column
	private int firstin;
	
	@Column
	private int firstout;
	
	@Column
	private int secondin;
	
	@Column
	private int secondout;
	
	public UserReportStatus() {
		// TODO Auto-generated constructor stub

	}

	public UserReportStatus(String today, int employee_id, int firstin, int firstout, int secondin, int secondout) {
		super();
		this.today = today;
		this.employee_id = employee_id;
		this.firstin = firstin;
		this.firstout = firstout;
		this.secondin = secondin;
		this.secondout = secondout;
	}

	public String getToday() {
		return today;
	}

	public void setToday(String today) {
		this.today = today;
	}

	public int getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}

	public int getFirstin() {
		return firstin;
	}

	public void setFirstin(int firstin) {
		this.firstin = firstin;
	}

	public int getFirstout() {
		return firstout;
	}

	public void setFirstout(int firstout) {
		this.firstout = firstout;
	}

	public int getSecondin() {
		return secondin;
	}

	public void setSecondin(int secondin) {
		this.secondin = secondin;
	}

	public int getSecondout() {
		return secondout;
	}

	public void setSecondout(int secondout) {
		this.secondout = secondout;
	}

	@Override
	public String toString() {
		return "UserReportStatus [today=" + today + ", employee_id=" + employee_id + ", firstin=" + firstin
				+ ", firstout=" + firstout + ", secondin=" + secondin + ", secondout=" + secondout + "]";
	}

	
}
