package projectX;

import java.util.LinkedList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import projectX.beans.Absence;
import projectX.daos.AbsenceDao;
import projectX.services.AbsenceService;
import projectX.services.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectXApplicationTests {

//	@Autowired
//	MockMvc tester;
	@MockBean
	UserService ud;
	@MockBean
	AbsenceService as;
	@MockBean
	AbsenceDao ad;
	@Test
	public void contextLoads() {
		try {
			Mockito.when(ad.findAllApprovedRequestByEmployeeId(1)).thenReturn(
					 new LinkedList<Absence>()
					);
//			MvcResult res = tester.perform(
//					MockMvcRequestBuilders.get("/absence/all_approved")
//					.accept(MediaType.APPLICATION_JSON)
//					).andReturn();
//			System.out.println(res.getResponse());
			Mockito.verify(ad).findAllApprovedRequestByEmployeeId(1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
